# destination-grenoble

Welcome to destination-grenoble, where I will publish my work as part of my stage at CHU Grenoble's Nephrology Unit!

This script was conceived to be run in RStudio. To use it, please install the RStudio IDE and open the file there.
* You will find that the script consists of two parts: a first block of code that does not knit to a file, but rather only calculates the necessary parameters and draws some graphs, and a second part that does knit to a file, presenting a table defining the parameters according to some groups
* To run the script, first execute the first block line-by-line, following the provided instructions to adequately define the number of groups to analyze.
* Once the groups are formed, run the second chunk of code by knitting the documment (Cntrl + K on RStudio) to generate a graphical summary of the results.

The results of this project can be found in [Pablo Marcos' Degree Thesis](http://www.pablomarcos.me/files/Memoria%20TFG%20FIRMADA.pdf), which is also availaible as [Open Access in the UPM's repository](https://oa.upm.es)

-----

<img src="https://codeberg.org/FlyingFlamingo/destination-grenoble/raw/branch/main/screenshot.png">
<div align="center">
Some of the graphs this script can generate
</div>